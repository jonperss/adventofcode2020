import openpyxl
import csv
from datetime import datetime
import collections

def findInvalidNumber(numbers):
    numbersToCheck = 25
    checkNums = numbers[:numbersToCheck]
    
    for i in range(numbersToCheck, len(numbers)):
        if not any(checkNums[first] + checkNums[second] == numbers[i] for first in range(numbersToCheck - 1) for second in range(first + 1, numbersToCheck)):
            return numbers[i]
        checkNums = checkNums[1:] + [numbers[i]]
        

with open('C:/Users/jonperss/Desktop/input.txt') as file:
    numbers = [int(x) for x in file.read().splitlines()]


print("Found first faulty number: " + str(findInvalidNumber(numbers)))
