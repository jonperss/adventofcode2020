import openpyxl
import csv
from datetime import datetime
import collections

def findSmallestAndLargestInRange(numbers, faultyNumber = 258585477):
    for i in range(0, len(numbers) - 1):
        currentSum = numbers[i]
        for j in range (i + 1, len(numbers)):
            currentSum += numbers[j]

            if(currentSum == faultyNumber):
                return (min(numbers[i : j+1]) + max(numbers[i : j+1]))
        

with open('C:/Users/jonperss/Desktop/input.txt') as file:
    numbers = [int(x) for x in file.read().splitlines()]


print("Found biggest and smallest number faulty number: " + str(findSmallestAndLargestInRange(numbers)))
