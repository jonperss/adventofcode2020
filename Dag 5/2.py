import openpyxl
import csv
from datetime import datetime


seatIds = []
mySeat = 0

boardingPasses = open("C:/Users/jonperss/Desktop/input.txt", "r").read().splitlines()


for boardingPass in boardingPasses:
    'Gör om strängen till ettor och nollor, konvertera till integer med basen 2'
    seatingRow = int(boardingPass[:7].replace("F", "0").replace("B", "1"), 2) 
    seatingCol = int(boardingPass[7:].replace("L", "0").replace("R", "1"), 2)

    seatIds.append((seatingRow * 8) + seatingCol)

print(max(seatIds))

'Måste använda Sorted() isäller för .sort() så den kan iterera - sätter None annars)'
seatIds = sorted(seatIds)

lastSeat = ''

for seat in seatIds:
    'Jämför två platser och se om den imellan är tom'
    if lastSeat and seat - lastSeat == 2:
        mySeat = seat - 1
        break
    lastSeat = seat

print(mySeat)
