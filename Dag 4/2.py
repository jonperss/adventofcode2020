import openpyxl
import csv
from datetime import datetime


approvedFields = 0
validPassPorts = 0
checkedPassports = 0
passPortFields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']

passPorts = open("C:/Users/jonperss/Desktop/input.txt", "r").read().split(sep='\n\n')


checkedPassports = 0

def passPortValid(dict):

    eyeColors = ['amb', 'blu','brn', 'gry', 'grn', 'hzl', 'oth']
    validCounter = 0
    
    if(1920 <= int(dict['byr']) <= 2002):
        validCounter += 1
        
    if(2010 <= int(dict['iyr']) <= 2020):
        validCounter += 1
        
    if(2020 <= int(dict['eyr']) <= 2030):
        validCounter += 1
        
    if('cm' in dict['hgt']):
        dict['hgt'] = dict['hgt'].replace('cm', "")
        if(150 <= int(dict['hgt']) <= 193):
                validCounter += 1
    else:
        dict['hgt'] = dict['hgt'].replace('in', '')
        if(59 <= int(dict['hgt']) <= 76):
            validCounter += 1
            
    if(dict['hcl'][0] == '#' and len(dict['hcl']) == 7):
        validCounter += 1
        
    for color in eyeColors:
        if(color == dict['ecl']):
            validCounter += 1
            
    if(len(dict['pid']) == 9):
        validCounter += 1

    if(validCounter == 7):
        return True
    else:
        return False
                
def splitPassports(passport):

    dict = {}
    newPass = passport.replace('\n' , ' ').split(' ')

    for field in newPass:
        splittedField = field.split(':')
        dict[splittedField[0]] = splittedField[1]

    if(passPortValid(dict)):
        global checkedPassports
        checkedPassports += 1                   

for passport in passPorts:
    for field in passPortFields:
        if field in passport:
            approvedFields += 1
            
    if approvedFields == 7:
        splitPassports(passport)
        validPassPorts += 1
        
    approvedFields = 0
print("Total checked : " + str(validPassPorts) + ", Total approved: " + str(checkedPassports))
    

        




