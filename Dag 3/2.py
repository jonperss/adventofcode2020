import openpyxl
import csv
from datetime import datetime

slope = []
tree = '#'
totalTreesHit = 1
treesHit = 0
startPos = 0
currentPos = 0 
rad = 0

slope = open("C:/Users/jonperss/Desktop/input.txt", "r").read().splitlines()

väg = [(1,1) , (3,1) , (5,1) , (7,1), (1,2)] #Ta in vägarna att gå 

for (höger, ner) in väg:

    while(startPos < len(slope) - 1): 
        startPos += ner
        rad = slope[startPos]
        currentPos += höger

        if(currentPos >= len(slope[0])): #Om vi gått hela vägen i x-led, börja om
           currentPos -= len(slope[0])

        if(rad[currentPos] == tree):
           treesHit += 1

    print("Trees hit this iteration: " + str(treesHit))
    totalTreesHit *= treesHit
    treesHit = 0
    startPos = 0
    currentPos = 0

print("You've hit " + str(totalTreesHit) + " Trees.")
