import openpyxl
import csv
from datetime import datetime
import collections


def findShinyGold(lines):
    rules = collections.defaultdict(list)
    for line in lines:
        left, right = line.split(' contain ')
        holder = parseColor(left)
        for contents in right.split(','):
            contained = parseColor(contents)
            rules[holder].append(contained)
    return countBags(rules, 'shiny gold')

def parseColor(colour):
    return colour.replace(' bags', '').replace(' bag', '').replace('.', '').strip()

def countBags(data, colour):
    if colour == 'other':
        return 0
    count = 0
    for cont in data[colour]:
        num_str = cont.split()[0]
        'Om det inte finns bags (dvs "no") -> Sätt num till 0 för att få ett nollvärde på nästa rekursion'
        if num_str == 'no':
            num = 0
        else:
            num = int(num_str)
        col = cont.split(maxsplit=1)[1]
        'Counter + x * bag -> gå vidare rekursivt om det fortfarande finns bags att kolla (exempel på sidan -> 1 guld har 7 bags i sig och 2 vibrant plum som i sin tur har 11 st i sig (1 + 1*7 + 2 + 2 * 11)' 
        count = count + num * (countBags(data, col) + 1)
    return count



    
with open('C:/Users/jonperss/Desktop/input.txt') as in_file:
    lines = in_file.read().strip().splitlines()
        
print(findShinyGold(lines))
