import openpyxl
import csv
from datetime import datetime
import collections


def findShinyGold(lines):
    rules = collections.defaultdict(list)
    for line in lines:
        left, right = line.split(' contain ')
        topBag = parseColor(left)
        for contents in right.split(','):
            contained = parseColor(contents[2:])
            rules[contained].append(topBag)
    return len(listHolders(rules, 'shiny gold'))


def parseColor(colour):
    return colour.replace(' bags', '').replace(' bag', '').replace('.', '').strip()


def listHolders(data, colour):
    result = set(data[colour])
    for col in data[colour]:
        'Gå "neråt" i varje set och merga med föregående iteration -> Union (typ SQLJoin)'
        result = result.union(listHolders(data, col))
    return result




    
with open('C:/Users/jonperss/Desktop/input.txt') as in_file:
    lines = in_file.read().strip().splitlines()
        
print(findShinyGold(lines))

