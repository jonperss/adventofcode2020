import openpyxl
import csv
from datetime import datetime
import collections

def findDuplicateExecution(lines):
    accumulator = 0
    currentLine = 0
    foundDuplicate = False
    quitIfElse = False
    
    while foundDuplicate == False:

        quitIfElse = False
        if(lines[currentLine][0] == "jmp"):
            if(lines[currentLine][2] == True):
                foundDuplicate = True
                break
            elif(lines[currentLine][1][0] == '+'):
                lines[currentLine][2] = True
                currentLine += int(lines[currentLine][1][1:])
                quitIfElse = True
            else:
                lines[currentLine][2] = True
                currentLine -= int(lines[currentLine][1][1:])
                quitIfElse = True
        elif(lines[currentLine][0] == 'acc' and quitIfElse != True):
            if(lines[currentLine][2] == True):
                foundDuplicate = True
                break
            elif(lines[currentLine][1][0] == '+'):
                accumulator += int(lines[currentLine][1][1:])
                lines[currentLine][2] = True
                currentLine += 1
                quitIfElse = True
            else:
                accumulator -= int(lines[currentLine][1][1:])
                lines[currentLine][2] = True
                currentLine += 1
                quitIfElse = True
        elif(lines[currentLine][0] == "nop" and quitIfElse != True):
            if(lines[currentLine][2] == True):
                foundDuplicate = True
                break
            else:
                lines[currentLine][2] = True
                currentLine += 1

        if(currentLine == len(lines)):
            print("Done!!, accumulator: " + str(accumulator))
            return True
        
    foundDuplicate = False
    return False

with open('C:/Users/jonperss/Desktop/input.txt') as in_file:
    lines = in_file.read().strip().splitlines()


for i in range(0, len(lines)):
    lines[i] = lines[i].split(' ')
    lines[i].append(False)

tmpLines = lines

print(findDuplicateExecution(lines))


for i in range(0, len(lines)):
    if(lines[i][0] == 'jmp'):
        lines[i][0] = 'nop'
        if(findDuplicateExecution(lines)):
            print("YES")
            break
        else:
            lines[i][0] = 'jmp'
    elif(lines[i][0] == 'nop'):
        lines[i][0] = 'jmp'
        if(findDuplicateExecution(lines)):
            print("YES")
            break
        else:
            lines[i][0] = 'nop'
    for line in lines:
        line[2] = False

